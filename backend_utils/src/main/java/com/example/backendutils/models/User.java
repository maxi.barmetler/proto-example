package com.example.backendutils.models;

import com.example.proto.UserMsg;
import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoField;

@Data
@ProtoClass(protoClass = UserMsg.class)
public class User {

    @ProtoField
    private String name;

    @ProtoField
    private int age;
}
