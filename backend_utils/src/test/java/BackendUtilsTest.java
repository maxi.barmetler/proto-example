import com.example.backendutils.ProtoDomainConverter;
import com.example.proto.UserMsg;
import com.example.backendutils.models.User;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class BackendUtilsTest {
    @Test
    public void test() {
        User user = new User();
        user.setName("John");
        user.setAge(25);
        UserMsg userMsg = ProtoDomainConverter.toProto(user);
        assertThat(userMsg.getName()).isEqualTo("John");
    }
}
