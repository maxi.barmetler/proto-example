package com.example.documentservice.converters;

import com.example.backendutils.ProtoDomainConverter;
import com.example.backendutils.models.User;
import com.example.proto.UserMsg;
import org.silbertb.proto.domainconverter.custom.TypeConverter;

public class UserConverter implements TypeConverter<User, UserMsg> {
    @Override
    public User toDomainValue(UserMsg protoValue) {
        return ProtoDomainConverter.toDomain(protoValue);
    }

    @Override
    public boolean shouldAssignToProto(User domainValue) {
        return domainValue != null;
    }

    @Override
    public UserMsg toProtobufValue(User domainValue) {
        return ProtoDomainConverter.toProto(domainValue);
    }
}
