package com.example.documentservice.models;

import com.example.backendutils.models.User;
import com.example.documentservice.converters.UserConverter;
import com.example.proto.DocumentMsg;
import lombok.Data;
import org.silbertb.proto.domainconverter.annotations.ProtoClass;
import org.silbertb.proto.domainconverter.annotations.ProtoConverter;
import org.silbertb.proto.domainconverter.annotations.ProtoField;

import java.util.List;

@Data
@ProtoClass(protoClass = DocumentMsg.class)
public class Document {

    @ProtoField
    private String name;

    @ProtoField
    private List<String> content;

    @ProtoField
    @ProtoConverter(converter = UserConverter.class)
    private User user;
}
