import com.example.documentservice.ProtoDomainConverter;
import com.example.documentservice.models.Document;
import com.example.proto.DocumentMsg;
import com.example.proto.UserMsg;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

import static com.example.backendutils.ProtoDomainConverter.toDomain;
import static com.example.documentservice.ProtoDomainConverter.toDomain;

public class DocumentServiceTests {
    @Test
    public void test() {
        ProtoDomainConverter.toProto(new Document());
        toDomain(UserMsg.newBuilder().build());
        toDomain(DocumentMsg.newBuilder().build());
    }
}
